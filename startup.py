import logging
import traceback

from ZhySu import ZhySuBot
from ZhySu.Utils import Configuration, Loggers, Database

# Setup Loggers
Loggers.setup_loggers()
log = logging.getLogger('ZhySu')

try:
    # Setup Database
    log.info('-------------------- INITIALISING --------------------')
    log.info('Connecting to Database...')
    Database.DBManager()
except Exception as error:
    log.critical('{0}: {1}\n{2}'.format(
        type(error).__name__,
        error,
        ''.join(traceback.format_exception(type(error), error, error.__traceback__))
    ))
    log.critical('Cannot start! Reason:\n{0}\nDetailed Traceback see above.'.format(str(error)))
    exit(1)

# Start Client
log.info('---------------- STARTING DISCORD BOT ----------------')
client = ZhySuBot()
client.run(Configuration().get('bot.token'))

# Cleanup
log.info('---------------------- CLEAN UP ----------------------')
log.info('Disconnecting from Database...')
Database.DBManager().get_database().close_all()

# Exit
log.info('----------------- EVERYTHING STOPPED -----------------')
