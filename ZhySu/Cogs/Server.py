import logging

import discord
from discord.ext import commands

from ZhySu import ZhySuBot
from ZhySu.Utils import Database, Colorize, Messages


class Server(commands.Cog):

    def __init__(self, bot: ZhySuBot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        db_guild = Database.Server(discord_id=guild.id)
        db_guild.save()

    @commands.Cog.listener()
    async def on_guild_remove(self, guild: discord.Guild):
        db_guild = Database.Server.get(Database.Server.discord_id == guild.id)
        db_guild.delete_instance()

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        db_guild = Database.Server.get(Database.Server.discord_id == member.guild.id)

        if db_guild.log_channel is not None:
            channel: discord.TextChannel = member.guild.get_channel(db_guild.log_channel)

            if channel is not None:
                await channel.send(embed=discord.Embed(
                    title=Messages.get('server.member.join.title', member.guild),
                    description=Messages.get('server.member.join.description', member.guild).format(member.mention),
                    color=await Colorize.get_user_color(member)
                ))

    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        db_guild = Database.Server.get(Database.Server.discord_id == member.guild.id)

        if db_guild.log_channel is not None:
            channel: discord.TextChannel = member.guild.get_channel(db_guild.log_channel)

            if channel is not None:
                await channel.send(embed=discord.Embed(
                    title=Messages.get('server.member.leave.description', member.guild),
                    description=Messages.get('server.member.leave.description', member.guild).format(member.mention),
                    color=await Colorize.get_user_color(member)
                ))


def setup(bot: ZhySuBot):
    bot.add_cog(Server(bot))
    logging.getLogger(__name__).info('Extension enabled')


def teardown(bot: ZhySuBot):
    bot.remove_cog('Test')
    logging.getLogger(__name__).info('Extension disabled')
