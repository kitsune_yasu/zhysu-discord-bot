import logging

import discord
from discord.ext import commands
from discord.ext.commands import Context, BucketType

from ZhySu import ZhySuBot
from ZhySu.Utils import Colorize, Messages


class Test(commands.Cog):

    def __init__(self, bot: ZhySuBot):
        self.bot = bot

    @commands.command(
        name='ping'
    )
    @commands.cooldown(1, 30, BucketType.user)
    @commands.guild_only()
    async def _ping(self, ctx: Context):
        async with ctx.typing():
            ping = round(self.bot.latency * 1000)
            logging.getLogger(__name__).info('{0.author.name} has asked for the ping ({1} ms).'.format(ctx, ping))
            await ctx.send(embed=discord.Embed(
                title=Messages.get('test.ping', ctx.guild),
                description='{0} ms'.format(ping),
                color=await Colorize.get_user_color(ctx.author)
            ).set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url))

    @commands.command(
        name='invite'
    )
    async def _invite(self, ctx: Context):
        logging.getLogger(__name__).info('{0.author.name} has asked for the invite.'.format(ctx))
        await ctx.send(embed=discord.Embed(
            title=Messages.get('test.invite', ctx.guild),
            description=discord.utils.oauth_url(self.bot.user.id, discord.Permissions(manage_roles=True, manage_channels=True, change_nickname=True,
                                                                                      read_messages=True, send_messages=True, manage_messages=True,
                                                                                      embed_links=True, attach_files=True, read_message_history=True,
                                                                                      mention_everyone=True, add_reactions=True)),
            color=await Colorize.get_user_color(ctx.author)
        ).set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url))


def setup(bot: ZhySuBot):
    bot.add_cog(Test(bot))
    logging.getLogger(__name__).info('Extension enabled')


def teardown(bot: ZhySuBot):
    bot.remove_cog('Test')
    logging.getLogger(__name__).info('Extension disabled')
