import datetime
import logging
import typing

import discord
from discord.ext import commands
from discord.ext.commands import Context

from ZhySu import ZhySuBot
from ZhySu.Utils import Messages


class Moderation(commands.Cog):

    def __init__(self, bot: ZhySuBot):
        self.bot = bot

    @commands.command(
        name='clear'
    )
    @commands.has_permissions(manage_messages=True)
    @commands.guild_only()
    async def _clear(self, ctx: Context, channel: typing.Optional[discord.TextChannel], number: int):
        if channel is None:
            channel = ctx.channel

        async with ctx.typing():
            await ctx.message.delete()
            oldest_possible_message = datetime.datetime.now() - datetime.timedelta(days=14)
            messages = await channel.history(limit=number, after=oldest_possible_message, oldest_first=False).flatten()
            await channel.delete_messages(messages)
            logging.getLogger(__name__).info('{0.author.name}#{0.author.discriminator} has deleted {1} messages in #{2}.'.format(
                ctx, len(messages), channel)
            )
            await ctx.send(
                embed=discord.Embed(
                    title=Messages.get('moderation.clear.title', ctx.guild).format(len(messages)),
                    description=Messages.get('moderation.clear.description', ctx.guild).format(len(messages), channel),
                    color=discord.Color.green()
                ).set_footer(text=Messages.get('global.requested', ctx.guild).format(ctx), icon_url=ctx.author.avatar_url),
                delete_after=30
            )


def setup(bot: ZhySuBot):
    bot.add_cog(Moderation(bot))
    logging.getLogger(__name__).info('Extension enabled')


def teardown(bot: ZhySuBot):
    bot.remove_cog('Moderation')
    logging.getLogger(__name__).info('Extension disabled')
