import logging

import coloredlogs

from ZhySu.Utils import Configuration
from logging.handlers import TimedRotatingFileHandler


def setup_loggers():

    # Setup Formatter
    log_formatter = logging.Formatter(
        Configuration().get('logs.format.log'),
        Configuration().get('logs.format.time'),
        Configuration().get('logs.format.style')
    )
    colored_formatter = coloredlogs.ColoredFormatter(
        Configuration().get('logs.format.log'),
        Configuration().get('logs.format.time'),
        Configuration().get('logs.format.style'),
        {
            'critical': {'bold': True, 'color': 'red'},
            'debug': {'color': 'green'},
            'error': {'color': 'red'},
            'info': {'color': 'cyan', 'bright': True},
            'notice': {'color': 'magenta'},
            'spam': {'color': 'green', 'faint': True},
            'success': {'bold': True, 'color': 'green'},
            'verbose': {'color': 'cyan', 'faint': True},
            'warning': {'color': 'yellow'}
        }
    )

    # Setup Handlers
    handlers = []

    if Configuration().get('logs.output.file.enabled'):
        file_handler = TimedRotatingFileHandler(
            Configuration().get('logs.output.file.folder') + '/' + Configuration().get('logs.output.file.prefix'),
            when='midnight',
            backupCount=Configuration().get('logs.output.file.backups'),
            encoding='utf-8'
        )
        file_handler.setFormatter(log_formatter)
        handlers.append(file_handler)
    if Configuration().get('logs.output.stdout.enabled'):
        stdout_handler = logging.StreamHandler()
        stdout_handler.setFormatter(colored_formatter)
        handlers.append(stdout_handler)

    # Setup Loggers
    for logger_name, logger_level in Configuration().get('logs.loggers').items():
        logger = logging.getLogger(logger_name)

        if logger_level.upper() == 'DEBUG':
            logger.setLevel(logging.DEBUG)
        elif logger_level.upper() == 'INFO':
            logger.setLevel(logging.INFO)
        elif logger_level.upper() == 'WARN' or logger_level.upper() == 'WARNING':
            logger.setLevel(logging.WARNING)
        elif logger_level.upper() == 'ERROR':
            logger.setLevel(logging.ERROR)
        elif logger_level.upper() == 'CRITICAL' or logger_level.upper() == 'FATAL':
            logger.setLevel(logging.CRITICAL)
        else:
            raise Exception("""
                Unsupported logging level "{0}" found for logger "{1}"
                Please use one of the following levels instead:
                DEBUG, INFO, WARN, WARNING, ERROR, CRITICAL, FATAL
                """.format(logger_level, logger_name))

        for handler in handlers:
            logger.addHandler(handler)
