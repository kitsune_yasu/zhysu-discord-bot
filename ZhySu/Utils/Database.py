import datetime

from peewee import Model, AutoField, BigIntegerField, DateTimeField, SQL, ForeignKeyField, CompositeKey, CharField, IntegerField, BooleanField
from playhouse.pool import PooledMySQLDatabase

from ZhySu.Utils import Configuration
from ZhySu.Utils.Metaclasses import Singleton

db = PooledMySQLDatabase(
    Configuration().get('database.database'),
    user=Configuration().get('database.username'),
    passwd=Configuration().get('database.password'),
    host=Configuration().get('database.host'),
    port=Configuration().get('database.port'),
    charset='utf8mb4'
)


class Server(Model):
    id = AutoField()
    discord_id = BigIntegerField(index=True)
    log_channel = BigIntegerField(null=True)
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Server, self).save(*args, **kwargs)

    def __repr__(self):
        return "<Server (id='{0}', discord_id='{1}', log_channel='{2}', created_at='{3}', updated_at='{4}')>".format(
            self.id, self.discord_id, self.log_channel, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'servers'
        database = db


class Member(Model):
    id = AutoField()
    discord_id = BigIntegerField(index=True)
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Member, self).save(*args, **kwargs)

    def __repr__(self):
        return "<Member (id='{0}', discord_id='{1}', created_at='{2}', updated_at='{3}')>".format(
            self.id, self.discord_id, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'members'
        database = db


class ServerMember(Model):
    server = ForeignKeyField(Server, backref='members', on_delete='CASCADE')
    member = ForeignKeyField(Member, backref='servers', on_delete='CASCADE')
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(ServerMember, self).save(*args, **kwargs)

    def __repr__(self):
        return "<ServerMember (server='{0}', member='{1}', created_at='{2}', updated_at='{3}')>".format(
            self.server, self.member, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'server_members'
        database = db
        primary_key = CompositeKey('server', 'member')


class Message(Model):
    id = AutoField()
    server = ForeignKeyField(Server, backref='messages', on_delete='CASCADE')
    key = CharField(max_length=255)
    value = CharField(max_length=1024)
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Message, self).save(*args, **kwargs)

    def __repr__(self):
        return "<Message (id='{0}', server='{1}', key='{2}', value='{3}', created_at='{4}', updated_at='{5}')>".format(
            self.id, self.server, self.key, self.value, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'messages'
        database = db


class Perm(Model):
    id = AutoField()
    server = ForeignKeyField(Server, backref='perms', on_delete='CASCADE')
    role = BigIntegerField()
    key = CharField(max_length=255)
    value = IntegerField()
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Perm, self).save(*args, **kwargs)

    def __repr__(self):
        return "<Perm (id='{0}', server='{1}', role='{2}', key='{3}', value='{4}', created_at='{5}', updated_at='{6}')>".format(
            self.id, self.server, self.role, self.key, self.value, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'perms'
        database = db


class Product(Model):
    id = AutoField()
    server = ForeignKeyField(Server, backref='products', on_delete='CASCADE')
    emoji = CharField(max_length=255)
    name = CharField(max_length=255)
    storage = IntegerField(constraints=[SQL("DEFAULT 0")])
    deliver_to = BigIntegerField()
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Product, self).save(*args, **kwargs)

    def __repr__(self):
        return "<Product (id='{0}', server='{1}', emoji='{2}', name='{3}', deliver_to='{4}', created_at='{5}', updated_at='{6}')>".format(
            self.id, self.server, self.emoji, self.name, self.deliver_to, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'products'
        database = db


class Subscription(Model):
    product = ForeignKeyField(Product, backref='subscriptions', on_delete='CASCADE')
    member = ForeignKeyField(Member, backref='subscriptions', on_delete='CASCADE')
    pings = BooleanField()
    count = IntegerField()
    hour = IntegerField()
    created = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(Subscription, self).save(*args, **kwargs)

    def __repr__(self):
        return "<Subscription (product='{0}', member='{1}', pings='{2}', count='{3}', hour='{4}', created_at='{5}', updated_at='{6}')>".format(
            self.product, self.member, self.pings, self.count, self.hour, self.created_at, self.updated_at
        )

    class Meta:
        table_name = 'subscriptions'
        database = db


class DBManager(metaclass=Singleton):
    __database: PooledMySQLDatabase = None

    def __init__(self):
        self.__database = db
        self.__database.connect()
        self.__database.create_tables([Server, Member, ServerMember, Message, Perm, Product, Subscription])

    def get_database(self) -> PooledMySQLDatabase:
        return self.__database
