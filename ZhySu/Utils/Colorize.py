from io import BytesIO

import discord
import typing
from asyncache import cached
from cachetools import TTLCache
from colorthief import ColorThief


async def get_user_color(user: typing.Union[discord.User, discord.Member]) -> discord.Color:
    return await get_asset_color(user.avatar_url_as(format='png'))


@cached(cache=TTLCache(maxsize=1024, ttl=3600))
async def get_asset_color(url: discord.Asset) -> discord.Color:
    color_thief = ColorThief(BytesIO(await url.read()))
    dominant_color = color_thief.get_color(quality=1)

    return discord.Color.from_rgb(dominant_color[0], dominant_color[1], dominant_color[2])
