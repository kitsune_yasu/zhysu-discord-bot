import logging
import typing
import random

import discord

from ZhySu.Utils import Configuration, Database


def get(key: str, server: typing.Optional[discord.Guild]):
    if server is not None:
        messages = Database.Message.select(Database.Message, Database.Server) \
            .join(Database.Server) \
            .where(Database.Server.id == server.id, Database.Message.key == key)

        if len(messages) > 0:
            return random.choice(messages).value

    if Configuration().exists('messages.{0}'.format(key)):
        msg = Configuration().get('messages.{0}'.format(key))
        if type(msg) is list:
            return random.choice(msg)
        else:
            return msg
    else:
        logging.getLogger(__name__).error('Could not find default message for key "{0}"'.format(key))
        return key
